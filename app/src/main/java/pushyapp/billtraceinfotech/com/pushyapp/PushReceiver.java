package pushyapp.billtraceinfotech.com.pushyapp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import me.pushy.sdk.Pushy;

public class PushReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        String notification_title = "BillTrace Pushy";
        String notification_message = "Message";

        //Attempt to get meesage from intent
        if (intent.getStringExtra("message") != null) {
            notification_message = intent.getStringExtra("message");
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.drawable.ic_arrow_drop_down_circle_black_24dp);
        builder.setContentTitle(notification_title);
        builder.setContentText(notification_message);
        builder.setLights(Color.RED, 1000, 1000);
        builder.setVibrate(new long[]{0, 400, 250, 400});
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));


        // Automatically configure a Notification Channel for devices running Android O+
        Pushy.setNotificationChannel(builder, context);

        // Get an instance of the NotificationManager service
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        // Build the notification and display it
        notificationManager.notify(1, builder.build());


    }
}
